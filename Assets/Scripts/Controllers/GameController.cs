﻿using System;
using Assets.Scripts.Creatures;
using Assets.Scripts.Creatures.Tanks;
using Assets.Scripts.View;
using Assets.Scripts.Weapons;
using UnityEngine;

namespace Assets.Scripts.Controllers
{
    public class GameController : MonoBehaviour
    {
        [Range(0, 50)] [SerializeField] private int _creaturesCount = 10;
        [SerializeField] private GuiGame _guiGame;
        [SerializeField] private Transform _weaponsHolder;
        [SerializeField] private Vector3[] _spawnPoints;

        public event Action<WeaponType> WeaponEquip = delegate { };

        public static GameController Instance;

        public ObjectsFactory.Factory Factory { private set; get; }
        public ResourcesController ResourcesController { private set; get; }

        public Vector3 AimPosition
        {
            get
            {
                return Camera.main.ScreenToWorldPoint(new Vector3(
                    _guiGame.GameWindow.AimTransform.rect.width, 
                    _guiGame.GameWindow.AimTransform.rect.height, 0));
            }
        }

        public Vector3 FarPosition { get { return new Vector3(-1000, -1000, -1000);  }}

        private Tank _tank;
        private CreaturesController _creaturesController;
        private WeaponsController _weaponsController;


        void Awake()
        {
            Instance = this;
            Factory = GetComponent<ObjectsFactory.Factory>();
            ResourcesController = GetComponent<ResourcesController>();
            _guiGame.MessageWindow.RetryPressed += StartLevel;
            InputController.Instance.EscapePressed += OnEscapePressed;
        }

        void Start()
        {
            StartLevel();
        }

        private void StartLevel()
        {
            _weaponsController = new WeaponsController(_weaponsHolder);
            _weaponsController.EquipWeapon += OnEquipWeapon;

            _tank = Factory.PoolOfTanks.Get(TankType.T55.ToString());
            _tank.Dead += OnTankDead;
            _tank.ReceivedDamage += OnTankReceivedDamage;
            _tank.Initialize(_guiGame.GameWindow.Health, _guiGame.GameWindow.Shield);

            _creaturesController = new CreaturesController(_creaturesCount, _tank.transform, _spawnPoints);
            _creaturesController.CreaturesKilled += OnCreaturesKilled;

            _guiGame.MessageWindow.Hide();
            _guiGame.GameWindow.Show();

            _weaponsController.Initialize();
        }

        private void OnTankReceivedDamage(int damage)
        {
            _guiGame.GameWindow.VisualizeDamage();
        }

        private void OnCreaturesKilled(int total)
        {
            _guiGame.GameWindow.CreaturesKilled = total;
        }

        private void ResetLevel()
        {
            _tank.Dead -= OnTankDead;
            _tank.ReceivedDamage -= OnTankReceivedDamage;
            Factory.PoolOfTanks.Put(_tank);
            _tank = null;

            _creaturesController.CreaturesKilled -= OnCreaturesKilled;
            _creaturesController.ResetLevel();

            _weaponsController.EquipWeapon -= OnEquipWeapon;

        }

        private void OnTankDead(Creature victim, ICreature killer)
        {
            _guiGame.MessageWindow.Show("YOU WERE KILLED BY " + killer.Name.ToUpper() + "!");
            _guiGame.GameWindow.Hide();
            ResetLevel();
        }

        private void OnEquipWeapon(WeaponType weaponType)
        {
            WeaponEquip(weaponType);
        }

        private void OnEscapePressed()
        {
            Application.Quit();
        }
    }
}
