﻿using System;
using UnityEngine;

namespace Assets.Scripts.Controllers
{
    public class InputController : MonoBehaviour
    {
        public event Action<int> Moved = delegate { };
        public event Action<int> Rotated = delegate { };
        public event Action<int> SwitchWeapon = delegate { };
        public event Action FirePressed = delegate { };
        public event Action EscapePressed = delegate { };

        public static InputController Instance;

        void Awake()
        {
            Instance = this;
        }

        private void CheckMoving()
        {
            if (Input.GetKey(KeyCode.DownArrow))
                Moved(-1);

            if (Input.GetKey(KeyCode.UpArrow))
                Moved(1);
        }

        private void CheckSwitch()
        {
            if (Input.GetKeyDown(KeyCode.Q))
                SwitchWeapon(-1);
            if (Input.GetKeyDown(KeyCode.W))
                SwitchWeapon(1);
        }

        private void CheckFire()
        {
            if (Input.GetKey(KeyCode.X))
                FirePressed();
        }

        private void CheckEscape()
        {
            if (Input.GetKey(KeyCode.Escape))
                EscapePressed();
        }

        private void CheckRotation()
        {
            if (Input.GetKey(KeyCode.LeftArrow))
                Rotated(-1);
            if (Input.GetKey(KeyCode.RightArrow))
                Rotated(1);
        }


        void Update()
        {
            CheckMoving();
            CheckSwitch();
            CheckFire();
            CheckRotation();
            CheckEscape();
        }
    }
}
