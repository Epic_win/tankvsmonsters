﻿using System.Collections.Generic;
using Assets.Scripts.Weapons;
using UnityEngine;

namespace Assets.Scripts.Controllers
{
    public class ResourcesController : MonoBehaviour
    {
        private const string WeaponIconsPath = "Weapons/WeaponIcons";
        private Sprite[] _weaponsSprites;

        void Awake()
        {
            _weaponsSprites = Resources.LoadAll<Sprite>(WeaponIconsPath);
        }

        public Sprite GetWeaponIcon(WeaponType type)
        {
            for (int i = 0; i < _weaponsSprites.Length; i++)
            {
                if (_weaponsSprites[i].name.Equals(type.ToString()))
                    return _weaponsSprites[i];
            }
            return null;
        }
    }
}
