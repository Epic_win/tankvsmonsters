﻿using System;
using Assets.Scripts.Controllers;
using Assets.Scripts.Factory;
using Assets.Scripts.View;
using Assets.Scripts.Weapons;
using UnityEngine;

namespace Assets.Scripts.Creatures
{
    public class Creature : MonoBehaviour, IPoolable
    {
        [SerializeField] private Transform _weaponHolder;

        public event Action<int> ReceivedDamage = delegate {  }; 
        public event Action<Creature, ICreature> Dead = delegate(Creature whoDead, ICreature byWho) {  };

        public ICreature LivingCreature { private set; get; }
        public bool InFight { protected set; get; }
        protected HealthView HealthView { set; get; }
        protected Weapon Weapon { set; get; }

        private ICreature _lastDamager;

        private Vector3 _scale = new Vector3(3.33f, 5, 1);

        private int _health;
        protected virtual int Health
        {
            get { return _health; }
            set
            {
                _health = Mathf.Clamp(value, 0, LivingCreature.MaxHealth);

                if (HealthView != null)
                    HealthView.UpdateHealth(_health);

                if (_health <= 0)
                {
                    Dead(this, _lastDamager);
                }
            }
        }

        protected virtual void Awake()
        {
            LivingCreature = GetComponent<ICreature>();
        }

        public void RecieveDamage(int damage, ICreature damager)
        {
            ReceivedDamage(damage);
            Health -= (int)(damage*(1f - LivingCreature.Shield));
            _lastDamager = damager;
        }

        protected void OnEquipWeapon(WeaponType weaponType)
        {
            if (Weapon != null)
                Unequip();

            Weapon = GameController.Instance.Factory.PoolOfWeapons.Get(weaponType.ToString());
            Weapon.transform.SetParent(_weaponHolder);
            Weapon.transform.localPosition = Vector3.zero;
            Weapon.transform.localRotation = Quaternion.identity;
            Weapon.transform.localScale = _scale;
            Weapon.Equip(this);
        }

        protected void Unequip()
        {
            if (Weapon == null) return;

            Weapon.Unequip();
            GameController.Instance.Factory.PoolOfWeapons.Put(Weapon);
            Weapon = null;
        }

        #region IPoolable
        public virtual string Type { get; private set; }

        public virtual void OnPutToPull()
        {
            Unequip();
            gameObject.SetActive(false);
        }

        public virtual void OnGetFromPull()
        {
            InFight = false;
            Health = LivingCreature.MaxHealth;
            gameObject.SetActive(true);
        }

        #endregion
    }
}
