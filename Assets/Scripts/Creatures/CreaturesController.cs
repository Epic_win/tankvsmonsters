﻿using System;
using System.Collections.Generic;
using Assets.Scripts.Controllers;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Assets.Scripts.Creatures
{
    public class CreaturesController
    {
        private int _creaturesKilled = 0;
        private List<Creature> _creatures = new List<Creature>();
        private string[] _creaturesId;
        private Transform _target;
        private Vector3[] _spawnPositions;

        public event Action<int> CreaturesKilled = delegate {  }; 

        public CreaturesController(int creaturesOnScene, Transform target, Vector3[] spawnPositions)
        {
            _spawnPositions = spawnPositions;
            _target = target;
            _creaturesId = Enum.GetNames(typeof(FollowingCreaturesType));

            for (int i = 0; i < creaturesOnScene; i++)
                AddCreature();
        }

        private void AddCreature()
        {
            var rndPos = _spawnPositions[Random.Range(0, _spawnPositions.Length)];
            var creature = GameController.Instance.Factory.PoolOfCreatures.Get(_creaturesId[Random.Range(0, _creaturesId.Length)]);
            creature.Dead += OnCreatureDead;
            creature.Initialize(_target, rndPos);
            _creatures.Add(creature);
        }

        private void RemoveCreature(Creature creature)
        {
            creature.Dead -= OnCreatureDead;
            GameController.Instance.Factory.PoolOfCreatures.Put((FollowingCreature)creature);
            _creatures.Remove(creature);
        }

        private void OnCreatureDead(Creature victim, ICreature killer)
        {
            RemoveCreature(victim);

            _creaturesKilled++;
            CreaturesKilled(_creaturesKilled);
            AddCreature();
        }

        public void ResetLevel()
        {
            Creature[] creatures = new Creature[_creatures.Count];
            _creatures.CopyTo(creatures);

            for (int i = 0; i < creatures.Length; i++)
                RemoveCreature(creatures[i]);

            _creaturesKilled = 0;
            CreaturesKilled(_creaturesKilled);

            _creatures.Clear();
        }
    }
}
