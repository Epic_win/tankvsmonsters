﻿using System.Collections;
using Assets.Scripts.Controllers;
using Assets.Scripts.View;
using Assets.Scripts.Weapons;
using UnityEngine;

namespace Assets.Scripts.Creatures
{
    public class FollowingCreature : Creature
    {
        private Transform _target;
        private IEnumerator _following;

        public void Initialize(Transform target, Vector3 spawnPosition)
        {
            StartCoroutine(SpawnDelay(spawnPosition));
            InitializeHealthView();
            _target = target;
        }

        private IEnumerator SpawnDelay(Vector3 spawnPosition)
        {
            yield return new WaitForSeconds(Random.Range(0, 2f));
            transform.position = new Vector3(spawnPosition.x, 0, spawnPosition.z);

            if (_following == null)
            {
                _following = Following();
                StartCoroutine(_following);
            }
        }

        private void InitializeHealthView()
        {
            if (HealthView == null)
            {
                HealthView = GameController.Instance.Factory.Create<HealthView>(typeof(HealthView).Name);
                HealthView.transform.SetParent(transform);
                HealthView.ResetTransform();
            }

            HealthView.Initialize(LivingCreature.MaxHealth);
        }

        void OnCollisionEnter(Collision other)
        {
            var creature = other.gameObject.GetComponent<Creature>();
            if (creature == null) return;

            InFight = creature.transform.Equals(_target);
            if (InFight)
                Weapon.Hit(creature);
        }

        void OnCollisionExit(Collision other)
        {
            InFight = false;
        }

        private IEnumerator Following()
        {
            while (_target != null)
            {
                if (!InFight)
                {
                    var targetPos = new Vector3(_target.position.x, 0, _target.position.z);
                    transform.LookAt(_target);
                    transform.position = Vector3.MoveTowards(transform.position, targetPos, Time.deltaTime * LivingCreature.MovementSpeed);
                }

                yield return null;
            }
            _following = null;
        }

        public override void OnGetFromPull()
        {
            transform.rotation = Quaternion.identity;
            transform.position = GameController.Instance.FarPosition;
            base.OnGetFromPull();
            OnEquipWeapon(WeaponType.Hand);
        }

        public override void OnPutToPull()
        {
            StopAllCoroutines();
            _target = null;
            _following = null;
            base.OnPutToPull();
        }
    }

    public enum FollowingCreaturesType
    {
        BrownCreature,
        Golem,
        SandCreature
    }
}