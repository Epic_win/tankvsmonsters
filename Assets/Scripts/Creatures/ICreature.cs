﻿namespace Assets.Scripts.Creatures
{
    public interface ICreature
    {
        string Name { get; }
        int MaxHealth { get; }
        float Shield { get; }
        float MovementSpeed { get; }
        int HandDamage { get; }
        float HandDamageCooldown { get; }
    }
}
