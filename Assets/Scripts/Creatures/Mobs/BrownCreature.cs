﻿namespace Assets.Scripts.Creatures.Mobs
{
    public class BrownCreature : FollowingCreature, ICreature
    {
        public string Name { get { return "Brown monster"; } }
        public int MaxHealth { get { return 60; } }
        public float Shield { get { return 0.2f; } }
        public float MovementSpeed { get { return 0.5f; } }
        public int HandDamage { get { return 5; } }
        public float HandDamageCooldown { get { return 1; } }

        public override string Type { get { return FollowingCreaturesType.BrownCreature.ToString(); } }


    }
}