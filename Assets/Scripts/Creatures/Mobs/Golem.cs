﻿namespace Assets.Scripts.Creatures.Mobs
{
    public class Golem : FollowingCreature, ICreature
    {
        public string Name { get { return "Golem"; } }
        public int MaxHealth { get { return 50; } }
        public float Shield { get { return 0.1f; } }
        public float MovementSpeed { get { return 1f; } }
        public int HandDamage { get { return 2; } }
        public float HandDamageCooldown { get { return 0.5f; } }

        public override string Type { get { return FollowingCreaturesType.Golem.ToString(); } }
    }
}