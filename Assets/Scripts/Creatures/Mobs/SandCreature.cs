﻿namespace Assets.Scripts.Creatures.Mobs
{
    public class SandCreature : FollowingCreature, ICreature
    {
        public string Name { get { return "Sand creature"; } }
        public int MaxHealth { get { return 80; } }
        public float Shield { get { return 0.3f; } }
        public float MovementSpeed { get { return 0.3f; } }
        public int HandDamage { get { return 7; } }
        public float HandDamageCooldown { get { return 1.2f; } }

        public override string Type { get { return FollowingCreaturesType.SandCreature.ToString(); } }
    }
}
