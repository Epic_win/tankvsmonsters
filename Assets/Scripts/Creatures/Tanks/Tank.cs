﻿using System.Collections;
using Assets.Scripts.Controllers;
using Assets.Scripts.View;
using UnityEngine;

namespace Assets.Scripts.Creatures.Tanks
{
    public class Tank : Creature
    {
        private Vector3 _targetPosition;
        private IEnumerator _moving;

        protected override void Awake()
        {
            base.Awake();
            GameController.Instance.WeaponEquip += OnEquipWeapon;
        }

        void Start()
        {
            InputController.Instance.Moved += OnMoved;
            InputController.Instance.FirePressed += OnFirePressed;
            InputController.Instance.Rotated += OnRotated;
        }

        public void Initialize(HealthView health, ShieldView shield)
        {
            HealthView = health;
            HealthView.Initialize(LivingCreature.MaxHealth);
            shield.Initialize(LivingCreature.Shield);
        }

        private void OnRotated(int side)
        {
            transform.Rotate(0, side, 0, Space.World);
            transform.eulerAngles = new Vector3(0, transform.eulerAngles.y, 0);
        }

        private void OnFirePressed()
        {
            if (Weapon == null) return;

            Weapon.Hit();
        }

        void OnCollisionEnter(Collision col)
        {
            if (col.gameObject.layer == 8) return;

            _targetPosition = transform.position;
        }

        private void OnMoved(int side)
        {
            _targetPosition = transform.forward * side + transform.position;
            if (_moving == null)
            {
                _moving = Moving();
                StartCoroutine(_moving);
            }
        }

        private IEnumerator Moving()
        {
            while ((_targetPosition - transform.position).magnitude > 0.01f)
            {
                transform.position = Vector3.MoveTowards(transform.position, _targetPosition, Time.deltaTime * LivingCreature.MovementSpeed);
                yield return null;
            }
            _moving = null;
        }

        public void Reset()
        {
            transform.position = Vector3.zero;
            OnPutToPull();
            OnGetFromPull();
        }

        public override void OnGetFromPull()
        {
            base.OnGetFromPull();
            transform.position = Vector3.zero;
        }
    }

    public enum TankType
    {
        T55,

    }
}