﻿namespace Assets.Scripts.Creatures.Tanks
{
    public class TankT55 : Tank, ICreature
    {
        public string Name { get { return "Tank t55"; } }
        public int MaxHealth { get { return 100; } }
        public float Shield { get { return 0.1f; } }
        public float MovementSpeed { get { return 3; } }
        public int HandDamage { get { return 5; } }
        public float HandDamageCooldown { get { return 1; } }

        public override string Type { get { return TankType.T55.ToString(); } }
    }
}
