﻿using Assets.Scripts.Creatures;
using Assets.Scripts.Creatures.Tanks;
using Assets.Scripts.Factory;
using Assets.Scripts.View;
using Assets.Scripts.Weapons;
using UnityEngine;

namespace Assets.Scripts.ObjectsFactory
{
    public class Factory : MonoBehaviour
    {
        [SerializeField] private GameObject[] _prefabs;

        public Pool<FollowingCreature> PoolOfCreatures = new Pool<FollowingCreature>();
        public Pool<Tank> PoolOfTanks = new Pool<Tank>();
        public Pool<Bullet> PoolOfBullets = new Pool<Bullet>();
        public Pool<Weapon> PoolOfWeapons = new Pool<Weapon>();
        public Pool<WeaponView> PoolOfWeaponViews = new Pool<WeaponView>();

        public T Create<T>(string type)
        {
            var prefab = GetPrefab<T>(type);
            return (T)Create<IPoolable>(prefab);
        }

        public T Create<T>(GameObject obj)
        {
            return Instantiate(obj).GetComponent<T>();
        }

        public GameObject GetPrefab<T>(string type)
        {
            for (int i = 0; i < _prefabs.Length; i++)
            {
                var poolable = _prefabs[i].GetComponent<IPoolable>();
                if (poolable == null) continue;
                if (!poolable.Type.Equals(type)) continue;
                var component = _prefabs[i].GetComponent<T>();
                if (component == null) continue;

                return _prefabs[i];
            }

            Debug.LogError("There is no prefab with type " + type);
            return null;
        }
    }
}
