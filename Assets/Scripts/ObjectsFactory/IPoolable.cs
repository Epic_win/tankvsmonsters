﻿namespace Assets.Scripts.Factory
{
    public interface IPoolable
    {
        string Type { get; }

        void OnPutToPull();
        void OnGetFromPull();
    }
}
