﻿using System.Collections.Generic;
using Assets.Scripts.Controllers;

namespace Assets.Scripts.Factory
{
    public class Pool<T> where T : IPoolable
    {
        private List<T> _container = new List<T>();

        public void Put(T obj)
        {
            obj.OnPutToPull();
            _container.Add(obj);
        }

        public T Get(string type)
        {
            IPoolable obj = null;

            for (int i = 0; i < _container.Count; i++)
            {
                if (!_container[i].Type.Equals(type)) continue;

                obj = _container[i];
                _container.RemoveAt(i);
                break;
            }

            if (obj == null)
                obj = GameController.Instance.Factory.Create<T>(type);

            obj.OnGetFromPull();
            return (T)obj;
        }
    }
}