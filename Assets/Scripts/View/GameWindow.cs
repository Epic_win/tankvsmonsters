﻿using System.Collections;
using Assets.Scripts.Weapons;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.View
{
    public class GameWindow : global::Assets.Scripts.View.View
    {
        [SerializeField] private float _damageScreenDuration = 0.5f;
        [SerializeField] private Text _killedCount;
        [SerializeField] private RectTransform _aimTransform;
        [SerializeField] private HealthView _healthView;
        [SerializeField] private ShieldView _shieldView;


        private Image _damageScreen;

        public HealthView Health { get { return _healthView; } }
        public ShieldView Shield { get { return _shieldView; } }
        public RectTransform AimTransform { get { return _aimTransform; } }
        public int CreaturesKilled { set { _killedCount.text = value.ToString(); } }

        void Awake()
        {
            _damageScreen = GetComponent<Image>();
            Close += OnClose;
        }

        private void OnClose()
        {
            StopAllCoroutines();
            _damageScreen.enabled = false;
        }

        public void VisualizeDamage()
        {
            _damageScreen.enabled = false;
            StartCoroutine(Visualizing());
        }

        private IEnumerator Visualizing()
        {
            _damageScreen.enabled = true;
            yield return new WaitForSeconds(_damageScreenDuration);
            _damageScreen.enabled = false;
        }
    
    }
}
