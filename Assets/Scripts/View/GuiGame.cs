﻿using UnityEngine;

namespace Assets.Scripts.View
{
    public class GuiGame : MonoBehaviour
    {
        [SerializeField] private GameWindow _gameWindow;
        public GameWindow GameWindow { get { return _gameWindow; } }

        [SerializeField] private MessageWindow _messageWindow;
        public MessageWindow MessageWindow { get { return _messageWindow; } }
    }
}
