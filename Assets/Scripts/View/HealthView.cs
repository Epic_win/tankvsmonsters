﻿using Assets.Scripts.Factory;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.View
{
    public class HealthView : global::Assets.Scripts.View.View, IPoolable
    {
        [SerializeField] private Image _healthImage;
        [SerializeField] private Text _healthText;

        private float _maxHealth;

        public void Initialize(int maxHealth)
        {
            _maxHealth = maxHealth;
            _healthImage.fillAmount = 1;
            _healthText.text = _maxHealth.ToString();
        }

        public void UpdateHealth(int current)
        {
            _healthText.text = current.ToString();
            _healthImage.fillAmount = current/ _maxHealth;
        }

        public void ResetTransform()
        {
            transform.localPosition = new Vector3(0, 1f, 0);
            transform.localScale = new Vector3(0.01f, 0.007f, 0);
        }

        public string Type { get { return GetType().Name; } }
        public void OnPutToPull()
        {
            gameObject.SetActive(false);
        }

        public void OnGetFromPull()
        {
            gameObject.SetActive(true);
        }
    }
}
