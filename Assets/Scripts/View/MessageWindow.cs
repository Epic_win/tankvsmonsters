﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.View
{
    public class MessageWindow : global::Assets.Scripts.View.View
    {
        [SerializeField] private Text _message;
        [SerializeField] private Button _retryButton;

        public event Action RetryPressed = delegate {  };

        void Awake()
        {
            _retryButton.onClick.AddListener(OnRetryPressed);
        }

        private void OnRetryPressed()
        {
            RetryPressed();
        }

        public void Show(string message)
        {
            _message.text = message;
            Show();
        }
    }
}
