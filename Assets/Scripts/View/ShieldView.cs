﻿using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.View
{
    public class ShieldView : global::Assets.Scripts.View.View
    {
        [SerializeField] private Image _shieldFilledImage;
        [SerializeField] private Text _shieldText;

        public void Initialize(float value)
        {
            _shieldText.text = (value * 100) + "%";
            _shieldFilledImage.fillAmount = value / 1f;
        }
    }
}
