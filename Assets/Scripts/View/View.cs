﻿using System;
using UnityEngine;

namespace Assets.Scripts.View
{
    public class View : MonoBehaviour
    {
        public event Action Open = delegate {  };
        public event Action Close = delegate {  };

        public void Show()
        {
            Open();
            gameObject.SetActive(true);
        }

        public void Hide()
        {
            Close();
            gameObject.SetActive(false);
        }
    }
}
