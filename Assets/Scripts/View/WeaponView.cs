﻿using Assets.Scripts.Controllers;
using Assets.Scripts.Factory;
using Assets.Scripts.Weapons;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.View
{
    public class WeaponView : MonoBehaviour, IPoolable
    {
        [SerializeField] private Image _baseImage;
        [SerializeField] private Image _selectedImage;

        public WeaponType WeaponType { get; private set; }

        private bool _selected;
        public bool Selected
        {
            get { return _selected; }
            set
            {
                _selectedImage.enabled = value;
                _selected = value;
            }
        }

        public void Initialize(WeaponType type, Sprite sprite)
        {
            WeaponType = type;
            _baseImage.sprite = sprite;
        }

        #region IPoolable
        public string Type { get { return "WeaponView"; } }
        public void OnPutToPull()
        {
            gameObject.SetActive(false);
        }

        public void OnGetFromPull()
        {
            Selected = false;
            gameObject.SetActive(true);
        }
        #endregion
    }
}
