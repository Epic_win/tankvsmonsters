﻿namespace Assets.Scripts.Weapons
{
    public class Automate : Weapon, IWeapon
    {
        public override string Type { get { return WeaponType.Automate.ToString(); } }
        public BulletType BulletType { get {return BulletType.Bullet;} }
        public int Damage { get { return 5; } }
        public float Cooldown { get { return 0.2f; } }
    }
}
