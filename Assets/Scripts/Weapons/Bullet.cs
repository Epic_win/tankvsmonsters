﻿using System;
using System.Collections;
using Assets.Scripts.Creatures;
using Assets.Scripts.Factory;
using UnityEngine;

namespace Assets.Scripts.Weapons
{
    public class Bullet : MonoBehaviour, IPoolable
    {
        private float _speed = 50;
        private IEnumerator _running;
        private Creature _collided;
        private Rigidbody _rigidbody;

        public event Action<Bullet, Collision> Hit = delegate {  };

        void Awake()
        {
            if (_rigidbody == null)
                _rigidbody = GetComponent<Rigidbody>();
        }

        public void Run(Vector3 startPosition, Vector3 direction)
        {
            transform.position = startPosition;
            transform.up = direction;
            if (_running == null)
            {
                _running = Running(direction);
                StartCoroutine(_running);
            }
        }

        IEnumerator Running(Vector3 direction)
        {
            while (_collided == null)
            {
                _rigidbody.velocity = direction*_speed;
                yield return null;
            }
            _running = null;
        }

        void OnCollisionEnter(Collision other)
        {
            Hit(this, other);
        }

        #region IPoolable
        public string Type { get { return BulletType.Bullet.ToString(); } }
        public void OnPutToPull()
        {
            _running = null;
            _collided = null;
            StopAllCoroutines();
            gameObject.SetActive(false);
        }

        public void OnGetFromPull()
        {
            gameObject.SetActive(true);

        }
        #endregion
    }
}
