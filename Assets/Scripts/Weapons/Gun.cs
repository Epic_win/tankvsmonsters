﻿namespace Assets.Scripts.Weapons
{
    public class Gun : Weapon, IWeapon
    {
        public override string Type { get { return WeaponType.Gun.ToString(); } }
        public BulletType BulletType { get { return BulletType.Bullet; } }
        public int Damage { get { return 20; } }
        public float Cooldown { get { return 1.5f; } }
    }
}
