﻿using System.Collections;
using Assets.Scripts.Creatures;
using UnityEngine;

namespace Assets.Scripts.Weapons
{
    public class Hand : Weapon, IWeapon
    {
        public override string Type { get { return WeaponType.Hand.ToString(); } }
        public BulletType BulletType { get { return BulletType.None; } }
        public int Damage { get { return Owner != null ? Owner.LivingCreature.HandDamage : 0; } }
        public float Cooldown { get { return Owner != null ? Owner.LivingCreature.HandDamageCooldown : 0; } }

        private IEnumerator _hitting;

        public override void Hit(Creature enemy)
        {
            if (enemy == null) return;

            Enemy = enemy;

            if (_hitting == null)
            {
                _hitting = Hitting();
                StartCoroutine(_hitting);
            }
        }

        private IEnumerator Hitting()
        {
            while (Owner.InFight && Enemy != null)
            {
                Enemy.RecieveDamage(Damage, Owner.LivingCreature);
                yield return new WaitForSeconds(Cooldown);
            }
            _hitting = null;
        }
    }
}
