﻿namespace Assets.Scripts.Weapons
{
    public interface IWeapon
    {
        BulletType BulletType { get; }
        int Damage { get; }
        float Cooldown { get; }
    }
}

