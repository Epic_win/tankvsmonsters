﻿using System;
using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.Controllers;
using Assets.Scripts.Creatures;
using Assets.Scripts.Factory;
using UnityEngine;

namespace Assets.Scripts.Weapons
{
    public class Weapon : MonoBehaviour, IPoolable
    {
        protected IWeapon WeaponData;
        protected Creature Owner;
        protected Creature Enemy;
        protected float LastTimeHit;

        private List<Bullet> _bullets = new List<Bullet>();

        void Awake()
        {
            WeaponData = GetComponent<IWeapon>();
        }

        public virtual void Hit(Creature enemy = null)
        {
            if ((Time.time - LastTimeHit) < WeaponData.Cooldown) return;

            AddBullet();
            LastTimeHit = Time.time;
        }

        private void AddBullet()
        {
            var bullet = GameController.Instance.Factory.PoolOfBullets.Get(WeaponData.BulletType.ToString());
            bullet.Hit += OnBulletHit;
            bullet.Run(GameController.Instance.AimPosition, Camera.main.transform.forward);
            _bullets.Add(bullet);
        }

        private void OnBulletHit(Bullet bullet, Collision collision)
        {
            var creature = collision.gameObject.GetComponent<Creature>();
            if (creature != null && Owner != null && creature != Owner)
                creature.RecieveDamage(WeaponData.Damage, Owner.LivingCreature);

            RemoveBullet(bullet);
        }

        private void RemoveBullet(Bullet bullet)
        {
            bullet.Hit -= OnBulletHit;
            _bullets.Remove(bullet);
            GameController.Instance.Factory.PoolOfBullets.Put(bullet);
        }

        public void Equip(Creature owner)
        {
            Owner = owner;
        }

        public void Unequip()
        {
            Reset();
        }

        private void Reset()
        {
            LastTimeHit = 0;
            Owner = null;
            if (_bullets.Count == 0) return;
            var bullets = _bullets;
            for (int i = 0; i < bullets.Count; i++)
                RemoveBullet(bullets[i]);
            _bullets.Clear();
        }

        #region IPoolable
        public virtual string Type { get; private set; }
        public void OnPutToPull()
        {
            gameObject.SetActive(false);
        }

        public void OnGetFromPull()
        {
            gameObject.SetActive(true);
        }
        #endregion
    }

    public enum WeaponType
    {
        Hand,
        Automate,
        Gun,

    }

    public enum BulletType
    {
        None,
        Bullet,
    }
}