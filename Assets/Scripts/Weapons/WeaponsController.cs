﻿using System;
using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.Controllers;
using Assets.Scripts.View;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Weapons
{
    public class WeaponsController
    {
        public event Action<WeaponType> EquipWeapon = delegate { };

        private List<WeaponView> _weaponViews = new List<WeaponView>();

        private WeaponView _current;
        private WeaponView Current
        {
            get { return _current; }
            set
            {
                if (_current != null)
                    _current.Selected = false;

                _current = value;
                _current.Selected = true;
                EquipWeapon(_current.WeaponType);
            }
        }

        public WeaponsController(Transform holder)
        {
            var weaponTypes = (WeaponType[])Enum.GetValues(typeof(WeaponType));
            InputController.Instance.SwitchWeapon += OnSwitchWeapon;

            for (int i = 0; i < weaponTypes.Length; i++)
            {
                var sprite = GameController.Instance.ResourcesController.GetWeaponIcon(weaponTypes[i]);
                if (sprite == null) continue;

                var view = GameController.Instance.Factory.PoolOfWeaponViews.Get("WeaponView");
                view.transform.SetParent(holder);
                view.Initialize(weaponTypes[i], sprite);
                _weaponViews.Add(view);
            }
        }

        public void Initialize()
        {
            Current = _weaponViews[0];
        }

        private void OnSwitchWeapon(int side)
        {
            var currentIndex = _weaponViews.IndexOf(Current);
            var newIndex = currentIndex + side;
            if (newIndex < 0)
                newIndex = _weaponViews.Count - 1;
            else if (newIndex >= _weaponViews.Count)
                newIndex = 0;

            Current = _weaponViews[newIndex];
        }
    }
}